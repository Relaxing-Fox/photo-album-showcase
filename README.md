# Photo Album Showcase

This showcase provides a the understanding how to build APIs from Python and using SQLite as a database of 5 columns and 5000 rows.

*  /api/v1/resources/photos/ => grabs all of the photos in the database.
*  /api/v1/resources/photos/{id}/ => grabs the ID of each photo in the database.
*  /api/v1/resources/photos/gui/ => Grabs the AlbumID of each photo and sets it into a simple SelectList from the database.
*  /api/v1/resources/photos/image/{id}/ => Grabs the thumbnailUrl from the database and displays  the Photo as an image.

**Database schema of Photos**
1.  [albumId] INT NULL
2.  [id] INT NULL
3.  [title] VARCHAR NULL
4.  [url] VARCHAR NULL
5.  [thumbnailUrl] VARCHAR NULL

Url and ThumbUrl are just placeholders of the Photos database.
Nothing special about them.

**Sample data**

albumId | id | title | url | thumbnailUrl |<br>
1   |	1	accusamus beatae ad facilis cum similique qui sunt  |	https://via.placeholder.com/600/92c952  |	https://via.placeholder.com/150/92c952 |<br>
1   |	2	reprehenderit est deserunt velit ipsam  |	https://via.placeholder.com/600/771796  |	https://via.placeholder.com/150/771796 |<br>
1   |	3	officia porro iure quia iusto qui ipsa ut modi  |	https://via.placeholder.com/600/24f355  |	https://via.placeholder.com/150/24f355 |<br>
1   |	4	culpa odio esse rerum omnis laboriosam voluptate repudiandae  | https://via.placeholder.com/600/d32776  |	https://via.placeholder.com/150/d32776 |<br>
1   |	5	natus nisi omnis corporis facere molestiae rerum in  |	https://via.placeholder.com/600/f66b97  |	https://via.placeholder.com/150/f66b97 |<br>
1   |	6	accusamus ea aliquid et amet sequi nemo  |	https://via.placeholder.com/600/56a8c2  |	https://via.placeholder.com/150/56a8c2 |<br>


### Tasks

1.  Display all the records from the table in a web browser. (Completed)
2.  Display each ID record from the table in a web browser. (Completed)
3.  Display each AlbumId record in a SelectList element from the table in a web browser. (Completed)
4.  Display each ThumbnailUrl as an image from the table in a web broswer. (Completed)

