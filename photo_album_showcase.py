import functools
import operator

import flask
from flask import jsonify
from sqlite3 import connect

app = flask.Flask(__name__)
app.config["DEBUG"] = True


def dict_factory(cursor, row):
    """ Returns items from the database as dictionaries rather than a list. It works better when used with JSON. """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def convert_tuple(tup):
    """ Converts a tuple to a string. """
    strings = functools.reduce(operator.add, tup)
    return strings


@app.route('/', methods=['GET'])
def home():
    """ Description of the Homepage. """
    return\
        '''
        <h1>Photos Database API</h1>
        <h3>This is a prototype API for understanding how to build APIs from Python.</h3>
        <p>
            To navigate the URLs, just copy and paste the URL after the port number (e.g. 5000).
        </p>
        <p>
            To navigate the IDs of the photos, append the number after the URL 
            (e.g. /api/v1/resources/photos/{id} => /api/v1/resources/photos/5)</p>
        </p>
        <ul>
            <li>/api/v1/resources/photos/ => Grabs all of the photos in the database.</li>
            <li>/api/v1/resources/photos/{id} => Grabs the ID of each photo in the database.</li>
            <li>/api/v1/resources/photos/gui => Grabs the AlbumID of each photo and sets it into a simple
                                                SelectList from the database.</li>
            <li>/api/v1/resources/photos/image/{id}/ => Grabs the thumbnailUrl from the database and displays 
                                                        the Photo as an image.</li>
        </ul>
        '''


@app.route('/api/v1/resources/photos/image/<id>/', methods=['GET'])
def api_img(id):
    """ Displays an image of the thumbnail as a URL. """
    conn = connect('photos.sqlite')
    conn.row_factory = dict_factory
    cur = conn.cursor()

    query = "SELECT thumbnailUrl FROM photos WHERE"
    to_filter = []

    if id:
        query += ' id=? AND'
        to_filter.append(id)
    if not id:
        return page_not_found(404)

    query = query[:-4] + ';'
    results = cur.execute(query, to_filter).fetchall()

    result = '''<img src="''' + str(results).replace('{', '').replace('}', '').replace('[', '').replace(']', '')\
             .replace('\'', '').replace('thumbnailUrl: ', '') + '''" />'''

    return result


@app.route('/api/v1/resources/photos/gui/', methods=['GET'])
def api_gui():
    """ Displays a SelectList of AlbumIds from the database. """
    conn = connect('photos.sqlite')
    cur = conn.cursor()
    conn.row_factory = dict_factory
    query = 'SELECT DISTINCT albumId FROM photos;'
    print(query)
    lst = []

    results = cur.execute(query).fetchall()
    string = convert_tuple(results)

    for res in string:
        photos = '''<option value="''' + str(res) + '''">''' + str(res) + '''</option>'''
        lst.append(photos)

    result = '''<p>Album Id:</p><select>''' + str(lst) + '''</select>'''

    return result


@app.route('/api/v1/resources/photos/', methods=['GET'])
def api_all():
    """ Displays all the attributes of the Photos database a as a JSON. """
    conn = connect('photos.sqlite')
    cur = conn.cursor()
    conn.row_factory = dict_factory
    all_photos = cur.execute('SELECT * FROM photos;').fetchall()

    return jsonify(all_photos)


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


@app.route('/api/v1/resources/photos/<id>/', methods=['GET'])
def api_filter(id):
    """ Displays each attribute of the Photos database as a JSON. """
    conn = connect('photos.sqlite')
    conn.row_factory = dict_factory
    cur = conn.cursor()

    query = "SELECT * FROM photos WHERE"
    to_filter = []

    if id:
        query += ' id=? AND'
        to_filter.append(id)
    if not id:
        return page_not_found(404)

    print(query)

    query = query[:-4] + ';'
    results = cur.execute(query, to_filter).fetchall()

    return jsonify(results)


# Starts the application
app.run()
